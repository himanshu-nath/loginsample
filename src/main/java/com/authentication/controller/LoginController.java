package com.authentication.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.authentication.bean.BaseResponse;
import com.authentication.bean.UserInfoBean;
import com.authentication.bean.UserInfoReadRS;
import com.authentication.enums.ResponseStatus;
import com.authentication.service.LoginService;

/**
 * Partner Login Controller 
 * @author Himanshu.Nath
 *
 */

@RestController
@RequestMapping("/user")
public class LoginController {
	private static final Logger log = Logger.getLogger(LoginController.class);
	
	@Autowired
	LoginService loginService; 

	/**
	 * APIs for user login via username and password
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "/signin", method = RequestMethod.GET)
	public ResponseEntity<UserInfoReadRS> userSignIn(
			@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "password", required = true) String password) {
		// http://localhost:8080/user/signin?username=hnath723&password=abcd@1234
		log.info("Inside userSignIn()");
		long startTime = System.currentTimeMillis();
		UserInfoReadRS userInfoReadRS = new UserInfoReadRS();
		try {
			if(username == null && password == null ) {
				userInfoReadRS.setStatus(ResponseStatus.FAILURE);
				userInfoReadRS.setMessage("Username & password is required");
				return new ResponseEntity<>(userInfoReadRS, HttpStatus.BAD_REQUEST);
			}
			
			UserInfoBean userInfoBean = loginService.userSignIn(username, password);
			if(userInfoBean != null) {
				userInfoReadRS.setUserinfoBean(userInfoBean);
				userInfoReadRS.setStatus(ResponseStatus.SUCCESS);
				userInfoReadRS.setMessage("User Found");
			} else {
				userInfoReadRS.setStatus(ResponseStatus.FAILURE);
				userInfoReadRS.setMessage("User Not Found");
			}			
		} catch (Exception e) {
			log.info("Unknown error while getting user info by login: " + e);
			userInfoReadRS.setStatus(ResponseStatus.ERROR);
			userInfoReadRS.setMessage("Error while fetching user info by login due to: "+e);
			return new ResponseEntity<>(userInfoReadRS, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.info("Time taken by LoginController : userSignIn - " + (System.currentTimeMillis() - startTime));
		return new ResponseEntity<>(userInfoReadRS, HttpStatus.OK);
	}
	
	/**
	 * API for user registration
	 * @param userInfoBean
	 * @return
	 */
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public ResponseEntity<BaseResponse> userSignUp(@RequestBody UserInfoBean userInfoBean) {
		// http://localhost:8080/user/signup
		log.info("Inside userSignUp()");
		long startTime = System.currentTimeMillis();
		BaseResponse baseResponse = new BaseResponse();
		try {
			if(userInfoBean == null) {
				baseResponse.setStatus(ResponseStatus.FAILURE);
				baseResponse.setMessage("All user info is required");
				return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
			}			
			baseResponse = loginService.userSignUp(userInfoBean);
		} catch (Exception e) {
			log.info("Unknown error while registring new user info: " + e);
			baseResponse.setStatus(ResponseStatus.ERROR);
			baseResponse.setMessage("Error while registring new user info by login");
			baseResponse.setDeveloperMessage("Error due to: "+e);
			return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.info("Time taken by LoginController : userSignUp - " + (System.currentTimeMillis() - startTime));
		return new ResponseEntity<>(baseResponse, HttpStatus.OK);
	}
	
	/**
	 * API to update user information
	 * @param userInfoBean
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<BaseResponse> updateUser(@RequestBody UserInfoBean userInfoBean) {
		// http://localhost:8080/user/update
		log.info("Inside updateUser()");
		long startTime = System.currentTimeMillis();
		BaseResponse baseResponse = new BaseResponse();
		try {
			if(userInfoBean == null) {
				baseResponse.setStatus(ResponseStatus.FAILURE);
				baseResponse.setMessage("User detail bean can't be null");
				return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
			}			
			baseResponse = loginService.updateUser(userInfoBean);
		} catch (Exception e) {
			log.info("Unknown error while updating user info: " + e);
			baseResponse.setStatus(ResponseStatus.ERROR);
			baseResponse.setMessage("Error while updating user info");
			baseResponse.setDeveloperMessage("Error due to: "+e);
			return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.info("Time taken by LoginController : updateUser - " + (System.currentTimeMillis() - startTime));
		return new ResponseEntity<>(baseResponse, HttpStatus.OK);
	}
	
	/**
	 * Remove user information by id
	 * @param userInfoBean
	 * @return
	 */
	@RequestMapping(value = "/update/{userId}", method = RequestMethod.DELETE)
	public ResponseEntity<BaseResponse> removeUser(@PathVariable("userId") Integer userId) {
		// http://localhost:8080/update/3
		log.info("Inside removeUser()");
		long startTime = System.currentTimeMillis();
		BaseResponse baseResponse = new BaseResponse();
		try {
			if(userId == null) {
				baseResponse.setStatus(ResponseStatus.FAILURE);
				baseResponse.setMessage("User Id can't be null");
				return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
			}			
			baseResponse = loginService.removeUser(userId);
		} catch (Exception e) {
			log.info("Unknown error while removing user info: " + e);
			baseResponse.setStatus(ResponseStatus.ERROR);
			baseResponse.setMessage("Error while removing user info by user id");
			baseResponse.setDeveloperMessage("Error due to: "+e);
			return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		log.info("Time taken by LoginController : removeUser - " + (System.currentTimeMillis() - startTime));
		return new ResponseEntity<>(baseResponse, HttpStatus.OK);
	}
}
