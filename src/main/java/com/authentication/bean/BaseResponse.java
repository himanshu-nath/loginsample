package com.authentication.bean;

import org.json.simple.JSONObject;
import com.authentication.enums.ResponseStatus;

/**
 * All REST response beans will extend this class, contains common attributes
 * @author Himanshu.Nath
 *
 */
public class BaseResponse {
	protected String status;
	protected String message;
	protected String developerMessage;
	protected String moreInfo;
	protected String apiVersion;
	protected int count;
	protected JSONObject jsonValue;

	public BaseResponse() {
		status = ResponseStatus.UNKNOWN.toString();
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(ResponseStatus status) {
		this.status = status.toString();
	}
	
	@Deprecated
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the developerMessage
	 */
	public String getDeveloperMessage() {
		return developerMessage;
	}

	/**
	 * @param developerMessage
	 *            the developerMessage to set
	 */
	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}

	/**
	 * @return the moreInfo
	 */
	public String getMoreInfo() {
		return moreInfo;
	}

	/**
	 * @param moreInfo
	 *            the moreInfo to set
	 */
	public void setMoreInfo(String moreInfo) {
		this.moreInfo = moreInfo;
	}

	/**
	 * @return the apiVersion
	 */
	public String getApiVersion() {
		return apiVersion;
	}

	/**
	 * @param apiVersion
	 *            the apiVersion to set
	 */
	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * @return the jsonValue
	 */
	public JSONObject getJsonValue() {
		return jsonValue;
	}

	/**
	 * @param jsonValue
	 *            the jsonValue to set
	 */
	public void setJsonValue(JSONObject jsonValue) {
		this.jsonValue = jsonValue;
	}
}
