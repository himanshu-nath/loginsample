package com.authentication.bean;

public class UserInfoReadRS extends BaseResponse{

	private UserInfoBean userinfoBean;

	/**
	 * @return the userinfoBean
	 */
	public UserInfoBean getUserinfoBean() {
		return userinfoBean;
	}

	/**
	 * @param userinfoBean the userinfoBean to set
	 */
	public void setUserinfoBean(UserInfoBean userinfoBean) {
		this.userinfoBean = userinfoBean;
	}
	
}
