package com.authentication.bean;

import java.io.Serializable;
import java.util.Date;
/**
 * 
 * @author Himanshu.Nath
 *
 */
public class UserInfoBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String email;
	private String role;
	private String password;
	private String username;
	private String token;
	private Date lastLogin;
	private Long dob;
	private String question1;
	private String question2;
	private String answer1;
	private String answer2;
	private Date joinDate;
	private byte activeIndex;
	private Date timecreated;
	private Date timemodified;	
	private Integer metaId;
	private Long mobileNo;
	private String firstName;
	private String lastName;
	private String gender;
	private String address;
	private String occupation;
	private Date metaTimecreated;
	private Date metaTimemodified;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}
	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the lastLogin
	 */
	public Date getLastLogin() {
		return lastLogin;
	}
	/**
	 * @param lastLogin the lastLogin to set
	 */
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	/**
	 * @return the dob
	 */
	public Long getDob() {
		return dob;
	}
	/**
	 * @param dob the dob to set
	 */
	public void setDob(Long dob) {
		this.dob = dob;
	}
	/**
	 * @return the question1
	 */
	public String getQuestion1() {
		return question1;
	}
	/**
	 * @param question1 the question1 to set
	 */
	public void setQuestion1(String question1) {
		this.question1 = question1;
	}
	/**
	 * @return the question2
	 */
	public String getQuestion2() {
		return question2;
	}
	/**
	 * @param question2 the question2 to set
	 */
	public void setQuestion2(String question2) {
		this.question2 = question2;
	}
	/**
	 * @return the answer1
	 */
	public String getAnswer1() {
		return answer1;
	}
	/**
	 * @param answer1 the answer1 to set
	 */
	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}
	/**
	 * @return the answer2
	 */
	public String getAnswer2() {
		return answer2;
	}
	/**
	 * @param answer2 the answer2 to set
	 */
	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}
	/**
	 * @return the joinDate
	 */
	public Date getJoinDate() {
		return joinDate;
	}
	/**
	 * @param joinDate the joinDate to set
	 */
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	/**
	 * @return the activeIndex
	 */
	public byte getActiveIndex() {
		return activeIndex;
	}
	/**
	 * @param activeIndex the activeIndex to set
	 */
	public void setActiveIndex(byte activeIndex) {
		this.activeIndex = activeIndex;
	}
	/**
	 * @return the timecreated
	 */
	public Date getTimecreated() {
		return timecreated;
	}
	/**
	 * @param timecreated the timecreated to set
	 */
	public void setTimecreated(Date timecreated) {
		this.timecreated = timecreated;
	}
	/**
	 * @return the timemodified
	 */
	public Date getTimemodified() {
		return timemodified;
	}
	/**
	 * @param timemodified the timemodified to set
	 */
	public void setTimemodified(Date timemodified) {
		this.timemodified = timemodified;
	}
	/**
	 * @return the metaId
	 */
	public Integer getMetaId() {
		return metaId;
	}
	/**
	 * @param metaId the metaId to set
	 */
	public void setMetaId(Integer metaId) {
		this.metaId = metaId;
	}
	/**
	 * @return the mobileNo
	 */
	public Long getMobileNo() {
		return mobileNo;
	}
	/**
	 * @param mobileNo the mobileNo to set
	 */
	public void setMobileNo(Long mobileNo) {
		this.mobileNo = mobileNo;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the occupation
	 */
	public String getOccupation() {
		return occupation;
	}
	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	/**
	 * @return the metaTimecreated
	 */
	public Date getMetaTimecreated() {
		return metaTimecreated;
	}
	/**
	 * @param metaTimecreated the metaTimecreated to set
	 */
	public void setMetaTimecreated(Date metaTimecreated) {
		this.metaTimecreated = metaTimecreated;
	}
	/**
	 * @return the metaTimemodified
	 */
	public Date getMetaTimemodified() {
		return metaTimemodified;
	}
	/**
	 * @param metaTimemodified the metaTimemodified to set
	 */
	public void setMetaTimemodified(Date metaTimemodified) {
		this.metaTimemodified = metaTimemodified;
	}
	
}
