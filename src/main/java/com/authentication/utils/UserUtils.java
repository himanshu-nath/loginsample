package com.authentication.utils;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.authentication.bean.UserInfoBean;
import com.authentication.pojo.Users;
import com.authentication.pojo.UsersMeta;

/**
 * 
 * @author Himanshu.Nath
 *
 */
@Component
public class UserUtils {

	private static final Logger log = Logger.getLogger(UserUtils.class);
	
	public Users createUser(UserInfoBean userInfoBean) {
		Users user = null;
		try {
			user = new Users();
			user.setEmail(userInfoBean.getEmail());
			user.setDob(userInfoBean.getDob());
			user.setRole(Constants.PARTNER);
			user.setJoinDate(DateUtils.getCurrentDateAndTimeInUTCTimeZone());
			user.setPassword(userInfoBean.getPassword());
			user.setUsername(userInfoBean.getUsername());
			user.setToken(CommonUtils.getUUID());
			user.setQuestion1(userInfoBean.getQuestion1());
			user.setQuestion2(userInfoBean.getQuestion2());
			user.setAnswer1(userInfoBean.getAnswer1());
			user.setAnswer2(userInfoBean.getAnswer2());
			user.setActiveIndex(Constants.ACTIVE);
			return user;
		} catch (Exception e) {
			log.info("Error in UserUtils : createUser mapping due to: "+e);
			return null;
		}
	}
	
	public UsersMeta createUserMeta(UserInfoBean userInfoBean, Users user) {
		UsersMeta usersMeta = null;
		try {
			usersMeta = new UsersMeta();
			usersMeta.setEmail(userInfoBean.getEmail());
			usersMeta.setAddress(userInfoBean.getAddress());
			usersMeta.setFirstName(userInfoBean.getFirstName());
			usersMeta.setLastName(userInfoBean.getLastName());
			usersMeta.setGender(userInfoBean.getGender());
			usersMeta.setMobileNo(userInfoBean.getMobileNo());
			usersMeta.setOccupation(userInfoBean.getOccupation());
			usersMeta.setUsers(user);
			return usersMeta;
		} catch (Exception e) {
			log.info("Error in UserUtils : createUserMeta mapping due to: "+e);
			return null;
		}
	}
	
	public UserInfoBean getUserDetails(Users user) {
		UserInfoBean userInfoBean = null;
		try {
			userInfoBean = new UserInfoBean();
			userInfoBean.setId(user.getId());
			userInfoBean.setDob(user.getDob());
			userInfoBean.setRole(user.getRole());
			userInfoBean.setJoinDate(user.getJoinDate());
			userInfoBean.setUsername(user.getUsername());
			userInfoBean.setActiveIndex(user.getActiveIndex());
			userInfoBean.setMetaId(user.getUsersMeta().getId());
			userInfoBean.setEmail(user.getEmail());
			userInfoBean.setAddress(user.getUsersMeta().getAddress());
			userInfoBean.setFirstName(user.getUsersMeta().getFirstName());
			userInfoBean.setLastName(user.getUsersMeta().getLastName());
			userInfoBean.setGender(user.getUsersMeta().getGender());
			userInfoBean.setMobileNo(user.getUsersMeta().getMobileNo());
			userInfoBean.setOccupation(user.getUsersMeta().getOccupation());
			userInfoBean.setTimecreated(user.getTimecreated());
			return userInfoBean;
		} catch (Exception e) {
			log.info("Error in UserUtils : getUserDetails mapping due to: "+e);
			return null;
		}
	}
	
	public UsersMeta updateUserMeta(UserInfoBean userInfoBean, Users user, UsersMeta usersMeta) {
		try {
			usersMeta.setEmail(userInfoBean.getEmail() != null ? userInfoBean.getEmail() : usersMeta.getEmail());
			usersMeta.setAddress(userInfoBean.getAddress() != null ? userInfoBean.getAddress() : usersMeta.getAddress());
			usersMeta.setFirstName(userInfoBean.getFirstName() != null ? userInfoBean.getFirstName() : usersMeta.getFirstName());
			usersMeta.setLastName(userInfoBean.getLastName() != null ? userInfoBean.getLastName() : usersMeta.getLastName());
			usersMeta.setGender(userInfoBean.getGender() != null ? userInfoBean.getEmail() : usersMeta.getGender());
			usersMeta.setMobileNo(userInfoBean.getMobileNo() != null ? userInfoBean.getMobileNo() : usersMeta.getMobileNo());
			usersMeta.setOccupation(userInfoBean.getOccupation() != null ? userInfoBean.getOccupation() : usersMeta.getOccupation());
			usersMeta.setUsers(user);
			return usersMeta;
		} catch (Exception e) {
			log.info("Error in UserUtils : createUserMeta mapping due to: "+e);
			return null;
		}
	}
}
