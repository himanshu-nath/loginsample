package com.authentication.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.authentication.controller.LoginController;

/**
 * 
 * @author Himanshu.Nath
 *
 */
public class DateUtils {
	
	public static Date getCurrentDateAndTimeInUTCTimeZone() {
		TimeZone.setDefault(TimeZone.getTimeZone(Constants.UTC_TIMEZONE));
		return new Date(Calendar.getInstance().getTimeInMillis());
	}
	
	public static Long getTimeStampInSecond() {
		return Calendar.getInstance().getTimeInMillis()/1000;
	}
}
