package com.authentication.utils;
/**
 * 
 * @author Himanshu.Nath
 *
 */
public class Constants {
	public static final String UTC_TIMEZONE = "UTC";
	public static final String NO_RECORD_FOUND = "No record found";
	
	public static final String ADMIN = "admin";
	public static final String PARTNER = "partner";
	
	public static final byte ACTIVE = 1;
	public static final byte INACTIVE = 0;
}
