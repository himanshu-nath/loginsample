package com.authentication.utils;

import java.util.UUID;
/**
 * 
 * @author Himanshu.Nath
 *
 */
public class CommonUtils {
	public static String getUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
}
