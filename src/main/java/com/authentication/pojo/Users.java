package com.authentication.pojo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.authentication.utils.DateUtils;

/**
 * users table
 * @author Himanshu.Nath
 *
 */

@Entity
@Table(name="users")
@NamedQuery(name="Users.findAll", query="SELECT u FROM Users u")
public class Users implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", columnDefinition = "INTEGER")
	private Integer id;

	@Column(name = "username", nullable = false, length = 64)
	private String username;
	
	@Column(name = "email", nullable = false, length = 64)
	private String email;
	
	@Column(name = "role", nullable = false, length = 64)
	private String role;
	
	@Column(name = "password", nullable = false, length = 64)
	private String password;
	
	@Column(name = "token", nullable = false, length = 64)
	private String token;
	
	@Column(name = "last_login", nullable = true, length = 64)
	private Date lastLogin;
	
	@Column(name = "dob", nullable = false, length = 64)
	private Long dob;
	
	@Column(name = "question_1", nullable = false, length = 64)
	private String question1;
	
	@Column(name = "question_2", nullable = false, length = 64)
	private String question2;
	
	@Column(name = "answer_1", nullable = false, length = 64)
	private String answer1;
	
	@Column(name = "answer_2", nullable = false, length = 64)
	private String answer2;
	
	@Column(name = "join_date", nullable = false, length = 64)
	private Date joinDate;
	
	@Column(name = "active_index", nullable = false, columnDefinition = "TINYINT")
	private byte activeIndex;

	@Column(name = "timecreated", nullable = false)
	private Date timecreated;

	@Column(name = "timemodified", nullable = false)
	private Date timemodified;	
	
	@OneToOne(mappedBy = "users")
	UsersMeta usersMeta;
	
	public Users() {
		super();
	}

	@PrePersist
	public void prePersist() {
		this.setTimecreated(DateUtils.getCurrentDateAndTimeInUTCTimeZone());
		this.setTimemodified(DateUtils.getCurrentDateAndTimeInUTCTimeZone());
	}

	@PreUpdate
	public void preUpdate() {
		this.setTimemodified(DateUtils.getCurrentDateAndTimeInUTCTimeZone());
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the lastLogin
	 */
	public Date getLastLogin() {
		return lastLogin;
	}

	/**
	 * @param lastLogin the lastLogin to set
	 */
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	/**
	 * @return the dob
	 */
	public Long getDob() {
		return dob;
	}

	/**
	 * @param dob the dob to set
	 */
	public void setDob(Long dob) {
		this.dob = dob;
	}

	/**
	 * @return the question1
	 */
	public String getQuestion1() {
		return question1;
	}

	/**
	 * @param question1 the question1 to set
	 */
	public void setQuestion1(String question1) {
		this.question1 = question1;
	}

	/**
	 * @return the question2
	 */
	public String getQuestion2() {
		return question2;
	}

	/**
	 * @param question2 the question2 to set
	 */
	public void setQuestion2(String question2) {
		this.question2 = question2;
	}

	/**
	 * @return the answer1
	 */
	public String getAnswer1() {
		return answer1;
	}

	/**
	 * @param answer1 the answer1 to set
	 */
	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	/**
	 * @return the answer2
	 */
	public String getAnswer2() {
		return answer2;
	}

	/**
	 * @param answer2 the answer2 to set
	 */
	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	/**
	 * @return the joinDate
	 */
	public Date getJoinDate() {
		return joinDate;
	}

	/**
	 * @param joinDate the joinDate to set
	 */
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	/**
	 * @return the activeIndex
	 */
	public byte getActiveIndex() {
		return activeIndex;
	}

	/**
	 * @param activeIndex the activeIndex to set
	 */
	public void setActiveIndex(byte activeIndex) {
		this.activeIndex = activeIndex;
	}

	/**
	 * @return the timecreated
	 */
	public Date getTimecreated() {
		return timecreated;
	}

	/**
	 * @param timecreated the timecreated to set
	 */
	public void setTimecreated(Date timecreated) {
		this.timecreated = timecreated;
	}

	/**
	 * @return the timemodified
	 */
	public Date getTimemodified() {
		return timemodified;
	}

	/**
	 * @param timemodified the timemodified to set
	 */
	public void setTimemodified(Date timemodified) {
		this.timemodified = timemodified;
	}

	/**
	 * @return the usersMeta
	 */
	public UsersMeta getUsersMeta() {
		return usersMeta;
	}

	/**
	 * @param usersMeta the usersMeta to set
	 */
	public void setUsersMeta(UsersMeta usersMeta) {
		this.usersMeta = usersMeta;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

}
