package com.authentication.pojo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.authentication.utils.DateUtils;

/**
 * users_meta table
 * @author Himanshu.Nath
 *
 */

@Entity
@Table(name="users_meta")
@NamedQuery(name="UsersMeta.findAll", query="SELECT um FROM UsersMeta um")
public class UsersMeta implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", columnDefinition = "INTEGER")
	private Integer id;

	@Column(name = "email", nullable = false, length = 64)
	private String email;
	
	@Column(name = "mobile_no", nullable = false)
	private Long mobileNo;
	
	@Column(name = "first_name", nullable = false, length = 64)
	private String firstName;
	
	@Column(name = "last_name", nullable = false, length = 64)
	private String lastName;
	
	@Column(name = "gender", nullable = false, length = 64)
	private String gender;
	
	@Column(name = "address", nullable = false, length = 64)
	private String address;
	
	@Column(name = "occupation", nullable = false, length = 64)
	private String occupation;
	
	@OneToOne
	@JoinColumn(name="user_id", referencedColumnName="id", nullable = false, columnDefinition = "INTEGER")
	private Users users;

	@Column(name = "timecreated", nullable = false)
	private Date timecreated;

	@Column(name = "timemodified", nullable = false)
	private Date timemodified;
	
	public UsersMeta() {
		super();
	}

	@PrePersist
	public void prePersist() {
		this.setTimecreated(DateUtils.getCurrentDateAndTimeInUTCTimeZone());
		this.setTimemodified(DateUtils.getCurrentDateAndTimeInUTCTimeZone());
	}

	@PreUpdate
	public void preUpdate() {
		this.setTimemodified(DateUtils.getCurrentDateAndTimeInUTCTimeZone());
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the mobileNo
	 */
	public Long getMobileNo() {
		return mobileNo;
	}

	/**
	 * @param mobileNo the mobileNo to set
	 */
	public void setMobileNo(Long mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the occupation
	 */
	public String getOccupation() {
		return occupation;
	}

	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	/**
	 * @return the users
	 */
	public Users getUsers() {
		return users;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(Users users) {
		this.users = users;
	}

	/**
	 * @return the timecreated
	 */
	public Date getTimecreated() {
		return timecreated;
	}

	/**
	 * @param timecreated the timecreated to set
	 */
	public void setTimecreated(Date timecreated) {
		this.timecreated = timecreated;
	}

	/**
	 * @return the timemodified
	 */
	public Date getTimemodified() {
		return timemodified;
	}

	/**
	 * @param timemodified the timemodified to set
	 */
	public void setTimemodified(Date timemodified) {
		this.timemodified = timemodified;
	}

}
