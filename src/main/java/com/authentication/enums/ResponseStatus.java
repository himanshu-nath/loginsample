package com.authentication.enums;
/**
 * All status messages can be used from this Enum
 * @author Himanshu.Nath
 *
 */
public enum ResponseStatus {
	SUCCESS("success"),
	WARNING("warning"),
	FAILURE("failure"),
	ERROR("error"),
	UNKNOWN("unknown");
	
	private String value;

	private ResponseStatus(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.value;
	}
}
