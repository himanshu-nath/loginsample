package com.authentication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.authentication.pojo.UsersMeta;

/**
 * 
 * @author Himanshu.Nath
 *
 */
@Repository
public interface UsersMetaRepository extends JpaRepository<UsersMeta, Integer> {
	
	@Query(value = "select * from users_meta where user_id = ?1", nativeQuery=true)
	public UsersMeta getUserMetaInfoByUserId(Integer userId);

}
