package com.authentication.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.authentication.pojo.Users;

/**
 * 
 * @author Himanshu.Nath
 *
 */
@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {

	@Query(value = "select * from users where username = ?1 and password = ?2", nativeQuery=true)
	public Users getUserInfoByUsernameAndPassword(String username, String password);
	
	@Transactional
    @Modifying(clearAutomatically =true)
	@Query(value="update users set token = ?1, last_login = ?3 where id = ?2",nativeQuery=true)
	public Integer updateTokenAndLastLoginById(String token, Integer id, Date lastLogin);

	public Users findById(Integer Id);
}
