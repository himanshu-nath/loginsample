package com.authentication.service;

import com.authentication.bean.BaseResponse;
import com.authentication.bean.UserInfoBean;

/**
* Login Authentication interface
* @author Himanshu.Nath
*
*/
public interface LoginService {

	/**
	 * Get user info by username and password
	 * @param username
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public UserInfoBean userSignIn(String username, String password) throws Exception;
	
	/**
	 * Register user method
	 * @param userInfoBean
	 * @return
	 * @throws Exception
	 */
	public BaseResponse userSignUp(UserInfoBean userInfoBean) throws Exception;
	
	/**
	 * Update user method
	 * @param userInfoBean
	 * @return
	 * @throws Exception
	 */
	public BaseResponse updateUser(UserInfoBean userInfoBean) throws Exception;
	
	/**
	 * Remove user method
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public BaseResponse removeUser(Integer userId) throws Exception;
}
