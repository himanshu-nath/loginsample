package com.authentication.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.authentication.bean.BaseResponse;
import com.authentication.bean.UserInfoBean;
import com.authentication.enums.ResponseStatus;
import com.authentication.pojo.Users;
import com.authentication.pojo.UsersMeta;
import com.authentication.repository.UserRepository;
import com.authentication.repository.UsersMetaRepository;
import com.authentication.utils.CommonUtils;
import com.authentication.utils.DateUtils;
import com.authentication.utils.UserUtils;

/**
 * 
 * @author Himanshu.Nath
 *
 */
@Service
public class LoginServiceImpl implements LoginService {
	private static final Logger log = Logger.getLogger(LoginServiceImpl.class);
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UsersMetaRepository usersMetaRepository;
	
	@Autowired
	UserUtils userUtils;
	
	/*
	 * Method to fetch user info by username and password
	 * @see com.authentication.service.LoginService#userSignIn(java.lang.String, java.lang.String)
	 */
	@Override
	public UserInfoBean userSignIn(String username, String password) throws Exception {
		UserInfoBean userInfoBean = null;
		
		Users user = null;
		//UsersMeta userMeta = null;
		String token = null;
		try {
			user = userRepository.getUserInfoByUsernameAndPassword(username, password);			
			//userMeta = usersMetaRepository.findOne(user.getUsersMeta().getId());
			if(user != null) {
				token = CommonUtils.getUUID();				
				userInfoBean = userUtils.getUserDetails(user);
				if(userRepository.updateTokenAndLastLoginById(token, user.getId(), DateUtils.getCurrentDateAndTimeInUTCTimeZone()) > 0)
					userInfoBean.setToken(token);
				else 
					userInfoBean.setToken(user.getToken());
				return userInfoBean;
			}			
		} catch(Exception e) {
			log.info("Error in LoginServiceImpl : userSignIn fetching user details due to: "+e);
			return null;
		}
		return null;
	}

	/*
	 * Method to register new user
	 * @see com.authentication.service.LoginService#userSignUp(com.authentication.bean.UserInfoBean)
	 */
	@Override
	public BaseResponse userSignUp(UserInfoBean userInfoBean) throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		Users user = null;
		UsersMeta userMeta = null;
		try {
			user = userUtils.createUser(userInfoBean);
			user = userRepository.save(user);
			
			userMeta = usersMetaRepository.save(userUtils.createUserMeta(userInfoBean, user));
			if(userMeta != null) {
				baseResponse.setStatus(ResponseStatus.SUCCESS);
				baseResponse.setMessage("User Created Successfully");
			} else {
				baseResponse.setStatus(ResponseStatus.FAILURE);
				baseResponse.setMessage("Failed To Create User");
				baseResponse.setDeveloperMessage("Please send all required information");
			}
		} catch(Exception e) {
			log.info("Error in LoginServiceImpl : userSignUp inserting user details due to: "+e);
			baseResponse.setStatus(ResponseStatus.ERROR);
			baseResponse.setMessage("Failed To Create User");
			baseResponse.setDeveloperMessage("Error due to: "+e);
			return baseResponse;
		}
		return baseResponse;
	}

	/*
	 * Method to update user
	 * @see com.authentication.service.LoginService#updateUser(com.authentication.bean.UserInfoBean)
	 */
	@Override
	public BaseResponse updateUser(UserInfoBean userInfoBean) throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		Users user = null;
		UsersMeta userMeta = null;
		try {
			user = userRepository.findById(userInfoBean.getId());			
			userMeta = usersMetaRepository.findOne(user.getUsersMeta().getId());
			
			userMeta = usersMetaRepository.save(userUtils.updateUserMeta(userInfoBean, user, userMeta));
			if(userMeta != null) {
				baseResponse.setStatus(ResponseStatus.SUCCESS);
				baseResponse.setMessage("User Details Updated Successfully");
			} else {
				baseResponse.setStatus(ResponseStatus.FAILURE);
				baseResponse.setMessage("Failed To Update User Details");
				baseResponse.setDeveloperMessage("Please send all required information");
			}
		} catch(Exception e) {
			log.info("Error in LoginServiceImpl : updateUser update user details due to: "+e);
			baseResponse.setStatus(ResponseStatus.ERROR);
			baseResponse.setMessage("Failed To update User");
			baseResponse.setDeveloperMessage("Error due to: "+e);
			return baseResponse;
		}
		return baseResponse;
	}

	/*
	 * Method to delete user by id
	 * @see com.authentication.service.LoginService#removeUser(java.lang.Integer)
	 */
	@Override
	public BaseResponse removeUser(Integer userId) throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		Users user = null;
		try {
			user = userRepository.findById(userId);
			if(user != null) {
				usersMetaRepository.delete(user.getUsersMeta().getId());
				userRepository.delete(user.getId());
				
				baseResponse.setStatus(ResponseStatus.SUCCESS);
				baseResponse.setMessage("User Details Deleted Successfully");
			} else {
				baseResponse.setStatus(ResponseStatus.FAILURE);
				baseResponse.setMessage("Failed To Deleted User Details");
				baseResponse.setDeveloperMessage("User id can't be null");
			}
		} catch(Exception e) {
			log.info("Error in LoginServiceImpl : removeUser delete user details due to: "+e);
			baseResponse.setStatus(ResponseStatus.ERROR);
			baseResponse.setMessage("Failed To delete User");
			baseResponse.setDeveloperMessage("Error due to: "+e);
			return baseResponse;
		}
		return baseResponse;
	}

}
